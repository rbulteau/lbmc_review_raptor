# LBMC Review - RAPToR

This repo holds the analyses done for the LBMC review article on RAPToR.


The analysis uses a previously published *C. elegans* profiling dataset:
> [**Two distinct transcription termination modes dictated by promoters**,  Miki et al., *Genes Dev.*  (2017)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5695088/)

available on GEO ([GSE97775](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE97775)).






## Analysis summary

We want to know if we can use RAPToR references to *detect* and/or *correct* for development in Differential Expression (DE) analyses, in cases where it is confounded with the variable of interest.

### In theory

To *detect* the overall impact of development, we can compare log-fold changes (logFC) between the samples groups with the logFC of the interpolated expression profiles of matching age in the reference. 
As the reference logFCs only correspond to development, we expect them to correlate with the sample logFCs when there is a difference in development between the compared groups, and we expect this correlation to increase with the developmental shift. 
Transcripts Per Million (TPMs) are more comparable across samples and datasets, so they are optimal for this comparison. 

To *correct* the effect of development is a more delicate endeavor. 
We must ‘interfere‘ with the standard DE analysis workflow, which relies on tools using counts as inputs for their particular statistical properties. 
To be able to join interpolated reference data to sample counts, we convert the artificial data from TPM to counts, assuming a fixed library size (25e06 counts). 
The artificial nature of the reference makes its properties (particularly the genes’ dispersion) different from those expected, and thus no statistical outputs from models including reference data are valid. 
We can however rely directly on the model coefficients (logFCs) estimated with the additional data to account for development.
We propose a model in which we include all reference expression profiles in a time window encompassing all of the compared samples. We can then model gene expression dynamics of development properly, and include the age of the samples in the model alongside variables of interest. 


### In practice

The dataset from *Miki et al.* is a timecourse profiling experiment of *C. elegans* wild-type worms and *xrn-2* mutants.
By picking samples from both timecourses, we can set up a gold standard of DE genes with samples of matching development.
Then, we can evaluate the effect of a developmental shift between the groups on the DE analysis by sliding the window of wild-type samples. 
We can do this for various intensities of developmental shifts.


#### Defining the gold standard and shifted WT sets

We selected 3 late time points in the timecourse with the best developmental match between WT and mutant. 
Fitting a simple model evaluating the effect of the strain with edgeR, we defined the gold standard of DE genes as those with an absolute logFC > 1 and a Bonferroni-Holm adjusted p.value < 0.05.

This represents 1315 genes.


Since we can't expect the same genes differ from the WT in the various timepoints of the mutant, we must shift the WT samples to emulate a developmental difference between the groups (thus, only 3 *xrn-2* mutant samples used in this analysis).

We shifted the set of 3 WT samples back 1, 2, 3, 5 and 7 timepoints.

<div align="center">
        <img src="./res/readme_figs/ae_chron_subsets.png" width="50%">
</div>


#### *Detecting* the effect of development
##### Performance of DE analysis drops with increasing developmental shift

As the developmental shift between our groups gets larger, the poorer our model performs at identifying genes from the gold standard.
This is to be expected as more genes change due to development with bigger shifts.

<div align="center">
        <img src="./res/readme_figs/PR_curves_std.png" width="50%"> 
        <img src="./res/readme_figs/nDEgene_barplot.png" width="25%">
</div>


##### logFCs correlate with the reference

While performance drops, the correlation of logFCs between sample groups and logFCs of the reference's age-matched profiles increases.
We get Pearson correlation scores reaching 0.9, which can be translated to over 80% (0.9²) of the variance in logFCs of the samples explained by the reference (*i.e.* development).

<div align="center">
        <img src="./res/readme_figs/logFC_cor_with_ref.png" width="70%">
</div>



#### *Correcting* the effect of development
##### A new model including reference data

To take development into account accurately in our model, we will directly integrate the reference data.
We select a window of the reference spanning the development of the selected samples +1h on each side to give us wiggle room.

Since expression dynamics can be non-linear, we model time as a spline and have to choose the number of degrees of freedom (df).
We find the optimal spline df of the selected reference window by fitting a spline model on it for a reasonable range of df values (2-8). 

<div align="center">
        <img src="./res/readme_figs/spline_df.png" width="30%"> 
</div>  

The smallest df which reaches the plateau of Residual Sum of Squares is kept for the final model.


The sample and reference data are then joined in a single count matrix, and an edgeR model is fit :  `~ ns(Age, df=df) + Batch + Strain`

We model expression dynamics with a spline of Age, Batch (reference/sample), and Strain (perturbation of interest) effects. This way, the strain coefficients estimated by the model take into account developmental changes dictated by the reference.

<div align="center">
        <img src="./res/readme_figs/model_cartoon.png" width="40%"> 
</div> 

Using the strain logFC directly as a classifier to detect DE gene already performs better than the standard analysis p.value for developmental shifts ≥ 2 timepoints (~ 3 hrs). 

Creating a compound index with a weighted mean between the standard analysis p.value and the strain logFC of the new model allows us to get the most of both approaches. 

``` 
CI = (1 – w)*-log10(p.value_std.analysis) + w * abs(strain.logFC_model with ref.)
```

The weight ratio of either classifier, ***w***, is proportional to the correlation of sample and reference logFC, as presented above. The optimal ***w*** values for each shift was defined as the value for which the Area under the P/R curve is maximal.

<div align="center">
        <img src="./res/readme_figs/opti_w.png" width="30%"> 
</div> 

At the optimal weight ratio, we found our compound index outperforms the standard analysis for all time-shifts considered, with larger time-shifts showing the strongest improvements. 
For the largest timeshift (WT -7), the optimal ***w*** value was 1, meaning none of the information from the standard DE model was kept in order to get the best results.

<div align="center">
        <img src="./res/readme_figs/PR_curves_opti.png" width="50%"> 
</div>        


### Conclusion

In summary, we have proven it is possible to detect, and resolve the confusion between development and perturbation by integrating RAPToR’s reference data in DE analysis. This not only means we can warn about the influence of development in results, but also renders previously unusable datasets exploitable.


## About the repo

### Structure

The scripts are split into 3 files :

 - Data download
 - Useful functions
 - Analysis (which calls the previous 2)
 

### Tools

R packages required :

 - [RAPToR](https://www.github.com/LBMC/RAPToR) v1.1.4
 - [wormRef](https://www.github.com/LBMC/wormRef) v0.4.0
 - GEOquery v2.54.1 (bioconductor) 
 - edgeR v3.28.1 (bioconductor)
 - ROCR v1.0-11
 - ggplot2 v3.3.1
 - ggpubr v0.3.0
 - viridis v0.5.1


